#!/usr/bin/env bash

# Stop on errors
set -e

echo "Executing check.sh"
git submodule update --init --recursive

# Get the folder name of _this_ script
SRCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

source ${MANIFEST}

# Get common stuff
source ${TARGET}/scripts/common/shell.sh

printf "${MAGENTA}"
message "Logging in"
printf "${NC}"
docker login registry.gitlab.com

printf "${MAGENTA}"
message "Checking if tag(s) exists"
printf "${NC}"
for i in "${!BUILDS[@]}"; do
  image_tag=$(echo "${BUILDS[$i]}" | awk -F'#' '{print $2}')
  # Check if it already exists.
  registry=$(echo "${image_tag}" | awk -F'/' '{print $1}')
  image=$(echo "${image_tag}" | awk -F'/' '{$1="";print $0}' | sed -e 's/^[[:space:]]*//' | tr ' ' '/' | awk -F':' '{print $1}')
  tag=$(echo "${image_tag}" | awk -F':' '{print $2}')
  set +e
  result=$(scripts/bin/docker_tags.sh -r ${registry} ${image} -t ${tag})
  code=$?
  set -e
  if [[ "${code}" -eq 0 ]]; then
    printf "${RED}${image_tag}${BLUE} already exists.\n"
    printf "${red}Not pushing!${NC}\n"
    exit 1
  else
    printf "${green}Ok ${cyan}to push.${NC}\n"
  fi
done
