#!/usr/bin/env bash

# Stop on errors
set -e

echo "Executing deploy.sh"
git submodule update --init --recursive
scripts/publish.sh
