#!/usr/bin/env bash

# Stop on errors
set -e

echo "Executing build.sh"
git submodule update --init --recursive
scripts/build.sh
