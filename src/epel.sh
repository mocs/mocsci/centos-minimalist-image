#!/usr/bin/env bash

if [[ "${arch}" == "armv7l" ]]; then
  echo "[epel]
name=Epel rebuild for armhfp
baseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/
enabled=1
gpgcheck=0" > "${centos_root}/etc/yum.repos.d/epel.repo"
fi
